
# fonte : 
# https://www.digitalocean.com/community/tutorials/como-fazer-scraping-em-paginas-web-com-beautiful-soup-and-python-3-pt

import requests
import csv
from bs4 import BeautifulSoup


import lxml.html as lh


# f = csv.writer(open('z-artist-names.csv', 'w'))
# f.writerow(['Name', 'Link'])

# pages = []

# for i in range(1, 5):
#     url = 'https://web.archive.org/web/20121007172955/https://www.nga.gov/collection/anZ' + str(i) + '.htm'
#     pages.append(url)


# for item in pages:
#     page = requests.get(item)
#     soup = BeautifulSoup(page.text, 'html.parser')

#     last_links = soup.find(class_='AlphaNav')
#     last_links.decompose()

#     artist_name_list = soup.find(class_='BodyText')
#     artist_name_list_items = artist_name_list.find_all('a')

#     for artist_name in artist_name_list_items:
#         names = artist_name.contents[0]
#         links = 'https://web.archive.org' + artist_name.get('href')

#         f.writerow([names, links])

f = csv.writer(open('nba_pts_season_2019_2020.csv', 'w'))
# f.writerow(['Position', 'Name', 'Points'])


print('Iniciando...')

url = 'http://www.espn.com/nba/history/leaders'


html_content = requests.get(url).text
# html_content = requests.get(url).content

soup = BeautifulSoup(html_content, "lxml")
# print(soup.prettify()) 

table = soup.find("table", attrs={"class": "tablehead"})

for tr in table.find_all("tr"):
    if (len(tr) == 3):
        position = ''
        name = '' 
        points = ''
        
        for idx, val in enumerate(tr.find_all("td")):
            if (idx == 0):
                position = val.text
            elif (idx == 1):
                name = val.text
            else:
                points = val.text

        f.writerow([position, name, points])


print('\n Finalizado!!!')